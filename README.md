# SEO-Analysis-Python 🐍

SEO-Analysis-Python

# imports:

    -flask
    -requests
    -bs4
    -nltk
    -ssl
    -socket
    -urllib.parse
    -collections

# usage

    -pip install -r requirements.txt
    -py main.py

    - access API in browser: http://127.0.0.1:5000/seo?url=<URL>

# logic

an SEO score will be given to the website based on the checks results.
the base score will be 100 and every bad check will deduct points from the score.
High Seo Impact -10, Med Seo Impact -5, Low Seo Impact -2.
toImprove are things that will help seo ranking, but won't affect overall score if missing.

# checks implemented

    -Check for the title -- HIGH IMPACT
    -Check for pages with more than one <title> tag -- HIGH IMPACT
    -Check for the meta description -- HIGH IMPACT
    -Check for header tags (H1,H2,H3,H4) -- HIGH IMPACT
    -Check for alt attributes on images -- MED IMPACT
    -Check for keywords and frequency (default language English, we need to implement a languages checker or get language input from user)
    -Check for word count -- HIGH IMPACT
    -Check for DOCTYPE declaration -- LOW IMPACT
    -Check for character encoding -- LOW IMPACT
    -Check for valid SSL certificate -- HIGH IMPACT
    -Check for SSL certificate that is about to expire -- LOW IMPACT
    -Check for Loading Time -- HIGH IMPACT
    -Check for 4xx error code -- MED IMPACT
    -Check for sitemap.xml -- HIGH IMPACT
    -Check for pages are blocked from appearing in search engines -- HIGH IMPACT
    -Check for pages with multiple meta description tags -- MED IMPACT
    -Check for pages with no content compression enabled -- MED IMPACT

# checks TO implement

    - ...
