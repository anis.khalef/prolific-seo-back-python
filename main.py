#############################################################
#############################################################
from flask import Flask, request, jsonify
from flask_cors import CORS                #
from collections import OrderedDict                         #
import re                                                   #
import time                                                 #
import ssl                                                  #
import socket                                               #
from datetime import datetime, timedelta                    #
import urllib.parse                                         #
import requests                                             #
from bs4 import BeautifulSoup                               #
import nltk
#
from nltk.tokenize import word_tokenize                     #
nltk.download('stopwords')                                  #
nltk.download('punkt')                                      #
#############################################################
#        USAGE: http://127.0.0.1:5000/seo?url=<URL>         #
#############################################################


def getSitemap(url: str) -> bool:
    if not url.startswith("http"):
        url = "http://" + url
    sitemap_url = url + "/sitemap.xml"
    response = requests.get(sitemap_url)
    if response.status_code == 200:
        return True
    else:
        return False


def getLoadTime(url):
    start_time = time.time()
    requests.get(url)
    end_time = time.time()

    load_time = end_time - start_time
    return load_time


def getDomain(url):
    parsed_url = urllib.parse.urlparse(url)
    domain = parsed_url.netloc.split(".")
    return ".".join(domain[-2:])


def getSSL(hostname: str, port: int = 443) -> bool:
    context = ssl.create_default_context()
    conn = context.wrap_socket(socket.socket(), server_hostname=hostname)
    try:
        conn.connect((hostname, port))
        cert = conn.getpeercert()
        return True
    except ssl.SSLError:
        return False


def isContentCompress(url):
    try:
        response = requests.get(url)
        if "content-encoding" in response.headers and response.headers["content-encoding"].lower() in ["gzip", "deflate"]:
            return True
        return False
    except requests.exceptions.RequestException:
        return False


def isSearchBlocked(url):
    if not url.startswith("http"):
        url = "http://" + url
    robots_url = url + "/robots.txt"
    try:
        response = requests.get(robots_url)
        if response.status_code == 200:
            content = response.content.decode("utf-8")
            for line in content.split("\n"):
                if line.startswith("User-agent: *"):
                    disallowed_paths = []
                    for disallow_line in content.split("\n")[content.split("\n").index(line):]:
                        if disallow_line.startswith("Disallow:"):
                            disallowed_path = re.search(
                                "Disallow:\s*(.*)", disallow_line).group(1)
                            disallowed_paths.append(disallowed_path)
                            if url.endswith(disallowed_path):
                                return True
                            if url.endswith("/"):
                                url = url[:-1]
                            if url.endswith(disallowed_path[:-1]):
                                return True
                    if not disallowed_paths:
                        return False
                else:
                    continue
        else:
            return False
    except requests.exceptions.RequestException:
        return False


app = Flask(__name__)


@app.route('/seo', methods=['GET'])
def seo_endpoint():
    url = request.args.get('url')
    if not url:
        return "Error: No URL provided", 400

    result = {}
    score = 100  # default
    wordCount = 0
    good = []
    missing = []
    toImprove = []
    response = requests.get(url)

    # unable to access website
    if response.status_code != 200:
        print("Error: Unable to access the website.")
        return

    soup = BeautifulSoup(response.content, 'html.parser')

    # Setup the connection to the url server
    hostname, port = getDomain(url), 443
    # print(hostname,port) connection log
    context = ssl.create_default_context()

    conn = context.wrap_socket(socket.socket(
        socket.AF_INET), server_hostname=hostname)
    conn.connect((hostname, port))

    # Check for the title
    title = soup.find('title')
    if title:
        good.append("title OK")
        title = title.get_text()

        # Check for multiple title tags
        title_tags = soup.find_all("title")
        if len(title_tags) > 1:
            missing.append("Multiple title tags detected, Only use one !")
            score -= 10
        else:
            good += ["Good, only one title tag detected. \n"]
    else:
        title = None
        missing += ["Title does not exist! Add a Title.\n"]
        score -= 10

    # Check for the meta description
    description = soup.find('meta', attrs={'name': 'description'})
    if description:
        description = description['content']
        good.append("Description OK")

        # Check for multiple meta description
        desc_tags = soup.find_all('meta', attrs={'name': 'description'})
        if len(desc_tags) > 1:
            missing.append(
                "Multiple meta description tags detected, Only use one !\n")
            score -= 5
        else:
            good.append("Good, only meta description title tag detected. ")
    else:
        description = None
        missing.append("Description does not exist! Add a Meta Description.\n")
        score -= 10

    # Check for header tags
    hs = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']
    h_tags = []
    for h in soup.find_all(hs):
        good.append(f"{h.name}-->{h.text.strip()}")
        h_tags.append(h.name)

    if 'h1' not in h_tags:
        missing.append("No H1 found!")
        score -= 10

    # Check for alt attributes on images
    images = soup.find_all('img')
    for i in images:
        if i.get('src').startswith('data:image/svg+xml'):
            continue  # skip SVG images

        if not i.get('alt'):
            missing.append(f"No Alt: {i.get('src')}")
            score -= 5

    # Check for keywords and frequency
    bod = soup.find('body').text
    words = [i.lower() for i in word_tokenize(bod)]
    sw = nltk.corpus.stopwords.words('english')
    new_words = []
    for i in words:
        if i not in sw and i.isalpha():
            new_words.append(i)
    freq = nltk.FreqDist(new_words)
    keywords = freq.most_common(10)

    # Check for word count
    bod = soup.find('body').text
    wordCount = len(word_tokenize(bod))
    if wordCount < 100:
        missing.append(
            f"Very Low word count! Try to aim for at least 300 words, you have {wordCount}\n")
        score -= 10
    if wordCount < 300:
        toImprove.append(
            f"To improve your SEO rating, Try to aim for at least 300 words, you have {wordCount}\n")
    if wordCount > 300:
        good.append(f"Good word count, you have {wordCount}\n")

    # Check for DOCTYPE declaration
    doctype = soup.find("<!DOCTYPE html>")
    doctype1 = soup.find("<!doctype html>")
    if not doctype and doctype1:
        missing.append("No DOCTYPE declaration found.\n")
        score -= 2
    else:
        good.append("Good DOCTYPE declared!\n")

    # Check for character encoding
    encoding = soup.find("meta", attrs={"http-equiv": "Content-Type"})
    if not encoding:
        missing.append("No character encoding declared\n")
        score -= 2
    else:
        encoding_declared = encoding.get("content").split("=")[1]
        good.append(f"Character encoding declared: {encoding_declared}")

    # check for valid SSL certificate
    if getSSL(getDomain(url)):
        good.append("Valid SSL Certificate detected.\n")
    else:
        missing.append("No Valid SSL Certificate was detected !\n")
        score -= 10

    # check the SSL expiration date
    ssl_info = conn.getpeercert()
    expires = datetime.strptime(ssl_info['notAfter'], '%b %d %H:%M:%S %Y %Z')
    remaining = expires - datetime.now()

    if remaining < timedelta(days=30):
        missing.append(
            f"\nSSL certificate is going to expire soon | Expiration date: {expires}\n"
        )
        score = -2
    else:
        good.append(
            f"\nSSL certificate is valid | Expiration date: {expires}\n")
    
    # check Load Time
    if getLoadTime(url) > 3:
        missing.append(
            f"\nThe website is taking too long to load. {round(getLoadTime(url), 4)} seconds\n"
        )
        score -= 10
    else:
        good.append(
            f"\nThe website loading time is OK. {round(getLoadTime(url), 4)} seconds\n"
        )
    loadtime = str(round(getLoadTime(url), 4))

    # check 4xx error code
    if response.status_code >= 400 and response.status_code < 500:
        missing.append(
            f"The URL returned a 4xx status code: {response.status_code}\n")
        score -= 5
    else:
        good.append(
            f"No 4xx status code. It returned {response.status_code}\n")

    # check for sitemap.xml
    if (getSitemap(getDomain(url))):
        good.append("\nsitemap.xml detected !\n")
    else:
        missing.append("\nsitemap.xml is missing !\n")
        score -= 10

    # check for pages are blocked from appearing in search engines
    if (isSearchBlocked(getDomain(url))):
        missing.append(
            "\nThis page is blocked from indexing in search engines ! Check robot.txt\n")
        score -= 10
    else:
        good.append("\nGood, this page is not blocked from search engines.\n")

    # check for pages with no content compression enabled
    if (isContentCompress(getDomain(url))):
        missing.append(
            "This page has content compression enabled. Please fix !\n")
        score -= 5
    else:
        good.append("Good, this page has no content compression enabled.\n")
    
    missing.append('')
    good.append('')
    """
    return jsonify({
    "title": title, 
    "description": description, 
    "keywords": keywords, 
    "loadtime": loadtime, 
    "good": good, 
    "missing": missing,
    "toImprove": toImprove,
    "score": score})
    """

    result = OrderedDict()

    result['title'] = title
    result['description'] = description
    result['keywords'] = keywords
    result['loadtime'] = loadtime
    result['good'] = good
    result['missing'] = missing
    result['toImprove'] = toImprove
    result['score'] = score

    return jsonify(result)


cors = CORS(app, resources={r"/*": {"origins": "*"}})

if __name__ == '__main__':
    app.run()
