# Utilisation de l'image python 3.8
FROM python:3.8

# Installation des dépendances nécessaires pour le projet
RUN apt-get update && \
    apt-get install -y python3-opencv && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Création du dossier de travail
WORKDIR /app

# Copie des fichiers de dépendances
COPY requirements.txt .

# Installation des dépendances
RUN  pip install   -r requirements.txt

# Copie des fichiers du projet
COPY . .

# Exposition du port utilisé par le serveur
EXPOSE 5000

CMD [ "python", "main.py" ]
# FROM python 

# WORKDIR /app

# COPY requirements.txt requirements.txt
# RUN pip install --upgrade pip
# RUN ["python", "-m", "pip", "install", "flask"]

# RUN pip3 install -r requirements.txt


# COPY . .

# CMD [ "python", "main.py" ]